# Repositorio de ejemplos de la EDU CIAA FPGA

## 1. Introducción

El objetivo de este repositorio es introducir al usuario de la EDU CIAA FPGA en el diseño, verificación y síntesis de circuitos digitales por medio de lenguajes de descripción de hardware. Los ejemplos se encuentran desarrollados en un orden de dificultad creciente para facilitar la gradualidad del aprendizaje.

Cada uno de los ejemplos es un proyecto que incluye, además del código fuente, una información detallada de las implicancias técnicas del diseño, el ensayo y en algunos casos la síntesis.

## 2. Índice de ejemplos

* [Compuerta AND](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/00-Compuerta_AND)
* [Comparador 1 bit](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/01-Comparador_1bit)
* [Multiplexor de 8 canales y N bits](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/02-MUX_8CH_nbits)
* [Comparador de N bits](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/03-Comparador_nbits)
* [Conversor BCD a 7 segmentos](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/04-BCD_a_7seg)
* [Flip flop D](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/05-FlipFlop_D)
* [Contador universal](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/06-Contador_universal)
* [Registro paralelo-paralelo](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/07-Registro_pp)
* [Registro serie-serie](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/08-Registro_ss)
* [Registro paralelo-serie](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/09-Registro_ps)
* [Registro serie-paralelo](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/10-Registro_sp)
* [Oscilador controlado numéricamente (NCO)](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/11-NCO)
* [Detector de trama](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/12_Detector_de_trama)
* [Circuito antirebote](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/13-Antirebote)
* [Árbitro](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/14-Arbiter)
* [Conversor BCD a 7 segmentos con testbench mejorado](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/16-BCD_a_7seg_TestbenchMejorado)
* [Oscilador controlado numericamente con LUT de salida](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/19-NCO_LUT)
* [Oscilador controlado numericamente con LUT de salida e interfaz serial](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/20-NCO_LUT_UART)
* [Periférico SPI](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/23-SPI)
* [Periférico serial](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/Puerto_COM)

## 3. Links de interés

* [Página oficial](https://bit.ly/EDUFPGAok)
* [Canal de Youtube](https://bit.ly/EDUFPGAyoutube)
* [Wiki](https://bit.ly/EDUFPGAwiki)
