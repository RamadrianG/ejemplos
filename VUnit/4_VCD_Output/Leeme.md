# Acerca de este ejemplo

Este ejemplo muestra como VUnit puede no solo compilar y ejecutar testbenches, sino tambien
simularlos y obtener formas de onda que luego visualizaremos a traves de la interfaz grafica.

## Partes importantes del codigo

El archivo **run.py** se mantiene invariante. En este caso, se ha agregado como DUT de ejemplo
un multiplexor de dos canales en el archivo **MUX.vhdl**:
```vhdl
library IEEE;
use IEEE.std_logic_1164.all;

entity MUX_2CH is
    generic(BITS_PER_CH : integer := 1);
    port(   ch0     : in  std_logic_vector(BITS_PER_CH-1 downto 0);
            ch1     : in  std_logic_vector(BITS_PER_CH-1 downto 0);
            sel     : in  std_logic;
            out_ch  : out std_logic_vector(BITS_PER_CH-1 downto 0)  );

end entity MUX_2CH ;

architecture ARQ_MUX_2CH of MUX_2CH is
signal zeros : std_logic_vector(BITS_PER_CH-1 downto 0);
begin
    zeros<=(others=>'0');
    process (ch0,ch1,sel) begin 
        case sel is
            when '0' => out_ch <= ch0;
            when '1' => out_ch <= ch1;
            when others => out_ch <= zeros;
        end case;
    end process;
end architecture ARQ_MUX_2CH ;
```

Ademas, se armo un testbench con distintos estimulos de prueba, respetando el
formato requerido por VUnit. Podran encontrar el codigo en **tb_MUX.vhdl**:
```vhdl
-- Librerias de VUnit para VHDL
library vunit_lib;
context vunit_lib.vunit_context;

-- Otras librerias
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb is
  -- VUnit necesita este string en TODOS los Testbench:
  generic (runner_cfg : string);
end tb;


architecture test of tb is

constant TIME_DELTA : time:=10 ns;

signal input    : std_logic_vector(15 downto 0);
signal selector : std_logic_vector(2 downto 0);
signal output1  : std_logic_vector(7 downto 0);

component MUX_2CH is
    generic(BITS_PER_CH : integer := 1);
    port(   ch0     : in  std_logic_vector(BITS_PER_CH-1 downto 0);
            ch1     : in  std_logic_vector(BITS_PER_CH-1 downto 0);
            sel     : in  std_logic;
            out_ch  : out std_logic_vector(BITS_PER_CH-1 downto 0)  );
end component;


begin

mux2 : MUX_2CH generic map(BITS_PER_CH=>8) 
               port map(ch0=>input(7 downto 0),
                        ch1=>input(15 downto 8),
                        sel=>selector(0),
                        out_ch=>output1 ) ;

    simulate: process
    begin

        -- Iniciar test VUnit
        test_runner_setup(runner, runner_cfg);

        -- Bombardear al DUT con estimulos
        input <= (others=>'0');
        selector <= (others=>'0');

        wait for TIME_DELTA;
        
        input <= "0000111100110101";

        wait for TIME_DELTA;
        selector <= "001";
        wait for TIME_DELTA;
        selector <= "010";
        wait for TIME_DELTA;
        selector <= "011";
        wait for TIME_DELTA;
        selector <= "100";
        wait for TIME_DELTA;
        selector <= "101";
        wait for TIME_DELTA;
        selector <= "110";
        wait for TIME_DELTA;
        selector <= "111";  
        wait for TIME_DELTA;
        wait for TIME_DELTA; --100 nseg

        -- Detener test VUnit
        test_runner_cleanup(runner);

    end process simulate;

end architecture test;

```

## Instrucciones de uso

Abrir un Terminal y ejecutar:
```bash
# Para una version Python 3.x, con x>=6:
python3.x run.py -g
```

La opcion -g le pide a VUnit que abra GTKWave despues de la simulacion. Alli podemos
navegar entre los testbenches, los modulos y las formas de onda simuladas. Normalmente
estaran estructurados en un arbol como el siguiente para agrupar las simulaciones de
todos los testbench:

```mermaid
graph TB;
 TOP-->Test_1;
 TOP-->Test_2;
 Test_1-->Waveforms;
 Test_1-->DUT;
 DUT-->Internal_Waveforms;
 DUT-->Internal_Components;
```
