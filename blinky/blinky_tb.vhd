library ieee;
use ieee.std_logic_1164.all;

entity blinky_tb is
end entity;

architecture blinky_tb_arch of blinky_tb is

--Declaración del componente
  component blinky is
  generic (T_PERIOD_MS : integer :=500);
  port(
          rst: in std_logic;
          clk: in std_logic;
          led_out: out std_logic
  );
end component;
  constant PERIOD 	   : time 	 := 83 ns;
  constant T_PERIOD_MS : integer := 10;
  signal rst,clk,led_out: std_logic;
  signal stop_simulation_s : BOOLEAN := FALSE;
begin

blinky0: blinky generic map (T_PERIOD_MS => 10)
                port map    (rst=>rst,clk=>clk,led_out=>led_out);

clockGeneration : process
	begin
	    clk <= '0';
	    wait for PERIOD/2;
	    clk <= '1';
	    wait for PERIOD/2;
      if (stop_simulation_s = TRUE) then
	        wait;
	    end if;
	end process clockGeneration;

testProcess : process
	begin
	    rst <= '0';
	    wait for PERIOD*5;
	    rst <= '1';
	    wait for (T_PERIOD_MS * 1 ms * 5);
      stop_simulation_s <= TRUE;
      wait;
	end process testProcess;

end architecture blinky_tb_arch;
