[[_TOC_]]

# Descripción

Este ejemplo muestra la implementación de un puerto serie en la FPGA que le permite comunicarse con la PC usando la conexión USB y la interfaz FTDI presente en la placa EDU-CIAA-FPGA.

**Autor:** Juan González Gómez (a.k.a. Obijuan)

# Diagrama en bloques

![Bloques](.images/Bloques.png)

El diagrama está formado por:
1. Un generador de pulsos cada 1 segundo.
2. Una cadena de texto _"Hello World !"_.
3. El pin de salida serie Tx, que en la placa está conectado al chip FTDI.

# Cómo probar el ejemplo

El procedimiento para probar este ejemplo se divide en dos partes:

## A - Descargar la configuración a la placa

Este proceso es similar a los ejemplos anteriores:
1. Abrir **iCEstudio**
2. Abrir el archivo **4_EDU_CIAA_FPGA.ice**
3. Conectar la **EDU-CIAA-FPGA** a la PC
4. **Verificar** el diseño (usando **Ctrl+R** o el botón en la esquina inferior derecha)
5. **Sintetizar** el diseño (usando **Ctrl+B** o el botón en la esquina inferior derecha)
6. **Cargar** el diseño en la placa (usando **Ctrl+U** o el botón en la esquina inferior derecha)

## B - Abrir un terminal serie en iCEstudio

1. En iCEstudio, ir a **Archivo > Plugins > Serial term**
2. En la pantalla del plugin, aplicar la siguiente configuracion para el puerto serie:

![SerialTerm_Config](.images/SerialTerm_1.png)

3. Hacer click en **Reload serial devices** y seleccionar el item **Dual_RS232-HS** en la lista de dispositivos:

![SerialTerm_List](.images/SerialTerm_2.png)

4. Hacer click en **Connect** para empezar a recibir informacion

![SerialTerm_List](.images/SerialTerm_3.png)

# Yo te propongo...
1. Cambiar la frecuencia con la que se envian mensajes a la PC
2. Cambiar el mensaje transmitido
3. Hacer doble click en el bloque **serial PRINT** para navegar su estructura interna

# Enlaces relacionados
 - [Ejemplo de descripcion y verificacion de un puerto serie en VHDL](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/Puerto_COM)