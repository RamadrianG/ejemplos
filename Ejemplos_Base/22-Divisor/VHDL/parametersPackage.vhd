library IEEE;
use IEEE.std_logic_1164.all;

package parametersPackage is
    constant NBITS : integer :=  8;
end parametersPackage;
