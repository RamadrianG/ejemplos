package parametersPackage is
    constant NCOBITS : integer :=  10;
    constant FREQCONTROLBITS : integer := 4;
    constant DATABITS : integer := 8;
end parametersPackage;
